Test project for using Moxy as an XML writer in Jersey.

To start the server:
./gradlew jettyRunWar

Then load:
http://localhost:8080/resttest/rest/services/json

You should see:
{"id":1,"name":"test","radius":3.0}

which demonstrates
1. MyMoxyContextResolver is being used to get and configure a Moxy JAXB context
2. The JAXB context has been initialized with the planets.oxm.xml mapping file

Load:
http://localhost:8080/resttest/rest/services/xml

You should see:
<planets><id>1</id><name>test</name><radius>3.0</radius></planets>


