package testing;

import javax.ws.rs.core.UriInfo;
import javax.xml.bind.Marshaller.Listener;

public class MarshallerListener extends Listener {
	
	private UriInfo uriInfo;

	public MarshallerListener(UriInfo uriInfo) {
		this.uriInfo = uriInfo;
	}
	
	@Override
	public void beforeMarshal(Object source) {
		System.out.println(source.getClass());
		System.out.println(source.getClass().getInterfaces());
		if (source instanceof Planet) {
			System.out.println(
					uriInfo.getBaseUriBuilder()
					.path(Services.class).
					build()
			);
		}
		else if (source instanceof Moon) {
			Moon moon = (Moon) source;
			System.out.println(
					uriInfo.getBaseUriBuilder()
					.path(Services.class)
					.path(Services.class, "moon")
					.resolveTemplate("moonid",moon.getName())
					.build()
			);
		}
	}

	@Override
	public void afterMarshal(Object source) {
		// TODO Auto-generated method stub
		super.afterMarshal(source);
	}
	
	

}
