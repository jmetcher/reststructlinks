package testing;

import org.eclipse.persistence.mappings.foundation.AbstractTransformationMapping;
import org.eclipse.persistence.mappings.transformers.FieldTransformer;
import org.eclipse.persistence.sessions.Session;

public class IdWriter implements FieldTransformer {

	@Override
	public void initialize(AbstractTransformationMapping mapping) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object buildFieldValue(Object instance, String fieldName,
			Session session) {
		return "id";
	}

}
