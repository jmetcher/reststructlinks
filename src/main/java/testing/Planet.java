package testing;

import java.util.ArrayList;
import java.util.List;

public class Planet {

    private int id = 1;
    private String name = "test";
    private double radius = 3.0;
    private String href;

    private List<Moon> moons = new ArrayList<Moon>(0);
    
    public void addMoon(Moon moon) {
    	moons.add(moon);
    }
}
