package testing;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.eclipse.persistence.mappings.foundation.AbstractTransformationMapping;
import org.eclipse.persistence.mappings.transformers.FieldTransformer;
import org.eclipse.persistence.sessions.Session;

public class HrefWriter implements FieldTransformer {
	
	@Override
	public Object buildFieldValue(Object instance, String fieldName,
			Session session) {
		return "href";
	}

	@Override
	public void initialize(AbstractTransformationMapping mapping) {
		// TODO Auto-generated method stub
		
	}

}
