package testing;

import javax.xml.namespace.QName;

import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.oxm.XMLDescriptor;
import org.eclipse.persistence.oxm.mappings.XMLTransformationMapping;

public class HrefCustomizer implements DescriptorCustomizer {

	@Override
	public void customize(ClassDescriptor descriptor) throws Exception {
        XMLTransformationMapping xtm = new XMLTransformationMapping();
        xtm.addFieldTransformer("@href", new HrefWriter());

        descriptor.addMapping(xtm);
        
	}

}
