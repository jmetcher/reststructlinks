package testing;

public class Moon {
	
	private String name;
	
	public Moon() {
	}
	
	public Moon(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
