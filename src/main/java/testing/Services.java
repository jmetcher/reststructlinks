package testing;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/services")
@Produces(MediaType.APPLICATION_XML)
public class Services {
	
	private Planet initPlanet() {
		Planet p = new Planet();
		p.addMoon(new Moon("moon1"));
		p.addMoon(new Moon("moon2"));
		return p;
	}

	@GET
	public Planet planet () {
		return initPlanet();
	}
	
	@GET @Path("/moons/{moonid}")
	public Moon moon (@PathParam("moonid") String name) {
		return new Moon(name);
	}
	
	@GET @Path("/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Planet planet2 () {
		return initPlanet();
	}
	

	
}
